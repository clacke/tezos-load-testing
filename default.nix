{ pkgs ? (import <nixpkgs> {}) }:
  let
    dontCheck = pkgs.haskell.lib.dontCheck;
    haskellPackages = pkgs.haskellPackages.override
    {
      overrides = self: super:
        {
          heist = dontCheck super.heist;
        };
    };
    inherit (haskellPackages) cabal cabal-install text process random aeson lens containers mtl bytestring async;
  in haskellPackages.callPackage (
{ mkDerivation, stdenv, haskellPackages}:
mkDerivation {
  pname = "tezos-loadtest";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [text process random aeson lens bytestring async];
  description = "Testing tool to generate transaction spam on sandbox networks.";
  license = stdenv.lib.licenses.bsd3;
  hydraPlatforms = stdenv.lib.platforms.none;
}) {}

