{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Main where

import Control.Concurrent.Async
import Control.Lens
import Control.Monad.RWS.Strict
import Data.Aeson hiding ((.=))
import Data.Foldable
import Data.Sequence (Seq)
import Data.Text (Text)
import Data.Void
import GHC.Generics
import GHC.IO.Exception
import System.Environment
import System.Process
import System.Random
import Text.Printf
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Sequence as Seq
import qualified Data.Text as T
import qualified Data.Text.IO as T


data ClientCommand a where
  GetBalanceFor :: ClientCommand ()
  Transfer :: ClientCommand ()
  LaunchDaemon :: ClientCommand Void-- ^ for now, we do all three -B -E -D
  WaitFor :: ClientCommand [Block]

newtype Ident = Ident {unIdent :: Text}
  deriving (Eq, Ord, Show, Generic, FromJSON)
newtype Operation = Operation Text
  deriving (Eq, Ord, Show, Generic, FromJSON)
newtype Block = Block Text
  deriving (Eq, Ord, Show, Generic, FromJSON)



data ClientState = ClientState
  { _clientState_ident :: Ident
  , _clientState_balance :: Double
  , _clientState_gen :: StdGen
  , _clientState_ops :: [Operation]
  }

data Node = Node
  { _node_host :: Text
  , _node_port :: Int
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Node


showTez :: Double -> String
showTez = printf "%.6f"

data ClientConfig = ClientConfig
  { _client_traders :: Seq Ident -- ^ identities to use as transferring traders
  , _client_bakers :: Seq Ident -- ^ identities to use as bakers
  , _client_nodes :: Seq Node -- ^ nodes to
  , _client_min_txn :: Double -- ^ value of transaction
  , _client_max_txn :: Double
  , _client_min_fee :: Double -- ^ value of fee awarded
  , _client_max_fee :: Double
  , _client_exe :: Text
  , _client_wait_confirmations :: Int
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON ClientConfig

makeLenses ''ClientConfig
makeLenses ''ClientState


type TestCase a = RWST ClientConfig () ClientState IO a

waitOneOp :: Operation -> TestCase Block
waitOneOp (Operation op) = do
  waits <- asks _client_wait_confirmations
  x <- tezosClient ["wait", "for", op, "to", "be", "included", "--confirmations", T.pack $ show waits]
  case x of
    (blkLine:_) -> return $ Block $ head $ tail $ T.splitOn (T.pack ":") blkLine

step :: ClientCommand a -> TestCase a
step cmd = do
  idents <- asks _client_traders
  minTxn <- asks _client_min_txn
  maxTxn <- asks _client_max_txn
  minFee <- asks _client_min_fee
  maxFee <- asks _client_max_fee

  ident <- gets _clientState_ident
  balance <- gets _clientState_balance
  gen <- gets _clientState_gen
  ops <- gets _clientState_ops

  let (qty, gen0) = randomR (minTxn, (min maxTxn balance)) gen
      (fee, gen1) = randomR (minFee, maxFee) gen0
      (rcptIndex, gen2) = (randomR (0, (length idents - 1)) gen1)
      recipient' = Seq.index idents rcptIndex
      recipient =
        if recipient' == ident
        then Seq.index idents (length idents - 1)
        else recipient'

  clientState_gen .= gen2

  case cmd of
    GetBalanceFor -> do
      balLine:[] <- tezosClient ["get", "balance", "for", unIdent ident]
      -- 4,004,516.3 ꜩ
      clientState_balance .= (read $ T.unpack $ T.replace "," "" $ head $ T.words balLine)

    Transfer -> do
      --  Operation successfully injected in the node.
      --  Operation hash is 'op5TiWpPb8nYzZd3SADDYmXxdCmREWSU1V27M3z2b9rg6uKDRGD'.
      (_:opLine:[]) <- tezosClient ["transfer", T.pack (showTez qty), "from", unIdent ident, "to", unIdent recipient, "--fee", T.pack (showTez fee)]
      let op = Operation $ head $ tail $ T.splitOn (T.pack "'") opLine
      clientState_ops %= (op:)
      clientState_balance -= fee
      clientState_balance -= qty

    WaitFor -> do
      -- Operation found in block: BMD6VeUoxV2vF2jmrSB3B8DUhmDPxjP4pEGEMoXVrbkYKNDz9KJ
      clientState_ops .= []
      traverse waitOneOp ops

    LaunchDaemon -> do
      tezosClient ["launch", "daemon", unIdent ident, "--baking", "--endorsement", "--denunciation"]
      error "not supposed to return"


tezosClient :: [Text] -> TestCase [Text]
tezosClient args = do
  tezosSandboxClient <- asks (T.unpack . _client_exe)
  gen <- gets _clientState_gen
  nodes <- asks _client_nodes

  let (nodeIdx, gen') = (randomR (0, (length nodes - 1)) gen)
      (Node host port) = Seq.index nodes nodeIdx

  clientState_gen .= gen'

  liftIO $ print (tezosSandboxClient, args)
  (exitCode, out, err) <- liftIO $ readProcessWithExitCode tezosSandboxClient
      ("--addr" : T.unpack host : "--port" : show port : fmap T.unpack args) ""

  case exitCode of
    ExitSuccess -> return $ T.pack <$> lines out
    _ -> error $ T.unpack $ T.concat ["client error: [", T.unwords args, "]\n", T.pack err]


genClients :: [Ident] -> StdGen -> [ClientState]
genClients [] g = []
genClients (x:xs) g = ClientState x 0 g0 [] : genClients xs g1
  where (g0, g1) = split g

setupClient = step GetBalanceFor

transferAndWait = void (step Transfer >> step WaitFor)


main :: IO ()
main = do
  (confPath:_) <- getArgs
  conf <- either error pure =<< eitherDecode <$> (liftIO $ LBS.readFile confPath)
  (gen0, gen1) <- split <$> getStdGen

  let bakers = runRWST (step LaunchDaemon) conf
        `mapConcurrently_` genClients (toList $ _client_bakers conf) gen0
  let traders = runRWST (setupClient >> forever transferAndWait) conf
        `mapConcurrently_` genClients (toList $ _client_traders conf) gen1

  concurrently_ bakers traders

